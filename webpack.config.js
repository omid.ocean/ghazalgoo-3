const webpack = require('webpack');
const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
  entry: ['webpack/hot/poll?1000', './src/main.hmr.ts'],
  watch: true,
  target: 'node',
  externals: [
    nodeExternals({
      whitelist: ['webpack/hot/poll?1000'],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  mode: 'development',
  resolve: {
    alias: {
      modules: path.resolve(__dirname, 'src', 'modules'),
      entity: path.resolve(__dirname, 'src', 'entity'),
      generics: path.resolve(__dirname, 'src', 'generics'),
      swagger: path.resolve(__dirname, 'src', 'swagger'),
      tools: path.resolve(__dirname, 'src', 'tools'),
      decorator: path.resolve(__dirname, 'src', 'decorator'),
    },
    extensions: ['.tsx', '.ts', '.js'],
  },
  plugins: [new webpack.HotModuleReplacementPlugin()],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'server.js',
  },
};
