import { Field, ObjectType } from 'type-graphql';

import { ApiProperty } from '@nestjs/swagger';

@ObjectType()
export class ErrorDTO {
  @Field()
  @ApiProperty()
  statusCode: number;

  @Field({ nullable: true })
  @ApiProperty()
  message: string;
}
