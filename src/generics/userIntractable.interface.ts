import { LikeAction } from 'entity/enum/likeAction.enum';
import { User } from 'entity/user.entity';

export interface IUserIntractable {
  addComment(id: number | string, user: User, commentText: string);

  like(id: number | string, user: User, action: LikeAction);

  likeComment(id: number | string, user: User, action: LikeAction);
}
