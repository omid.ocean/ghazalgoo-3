import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class PageInfo {
  @Field({ nullable: true })
  endCursor: number;

  @Field({ nullable: false })
  hasNextPage: boolean;

  @Field({ nullable: false })
  hasPreviousPage: boolean;

  @Field({ nullable: true })
  startCursor: number;
}
