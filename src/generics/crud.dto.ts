import { Field, ObjectType } from 'type-graphql';

import { ApiProperty } from '@nestjs/swagger';

@ObjectType()
export class UpdateAndDelete {
  @Field({ nullable: true })
  @ApiProperty()
  affected: number;
}
