import { ClassType, Field, Int, ObjectType } from 'type-graphql';

import { PageInfo } from './pageInfo.dto';

export default function PaginatedResponse<TItem>(TItemClass: ClassType<TItem>) {
  @ObjectType({ isAbstract: true })
  abstract class PaginatedResponseClass {
    @Field(()=> [TItemClass])
    nodes: TItem[];

    @Field(() => PageInfo)
    pageInfo: PageInfo;

    @Field(()=> Int)
    totalCount: number;
  }
  return PaginatedResponseClass;
}
