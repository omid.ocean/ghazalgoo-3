import {
    AbstractRepository, Between, FindOneOptions, LessThan, LessThanOrEqual, Like, MoreThan,
    MoreThanOrEqual
} from 'typeorm';

import { BadRequestException, NotFoundException } from '@nestjs/common';

import { User } from '../entity/user.entity';
import { IAddAttr } from '../tools/IAddAttr.interface';

export abstract class RepoBase<T> extends AbstractRepository<T> {
  /**
   * finds item by provided id
   * @throws NotFoundException
   * @param id numerical id
   * @param fields graphql fields
   * @returns found item
   */
  async findById(id: number, fields: IAddAttr) {
    const item = await this.repository.findOne(id, {
      relations: fields.relations,
      select: fields.select,
    } as any);
    if (!item) {
      throw new NotFoundException(
        'the item with the provided id does not exist!',
      );
    }
    return item;
  }

  async findBySlug(fields: FindOneOptions<T>) {
    const item = await this.repository.findOne({
      ...fields,
    } as any);

    if (!item) {
      throw new NotFoundException(
        'the item with the provided id does not exist!',
      );
    }
    return item;
  }

  protected async abstractGetAllPaginated(
    startCursor: number,
    endCursor: number,
    limit: number,
    search: string,
    fields: IAddAttr,
    searchField: string,
  ) {
    if (limit < 1) {
      throw new BadRequestException('items limit must be greater than 1');
    }
    if (limit > 100) {
      throw new BadRequestException('items limit must be less than 100');
    }

    const whereCondition = {} as any;
    if (endCursor && startCursor) {
      whereCondition.id = Between(endCursor, startCursor);
    } else if (startCursor) {
      whereCondition.id = LessThan(startCursor);
    } else if (endCursor) {
      whereCondition.id = MoreThan(endCursor);
    }
    if (search) {
      whereCondition[searchField] = Like(`%${search}%`);
    }

    const totalCount = await this.repository.count({
      where: search ? { [searchField]: Like(`%${search}%`) } : '',
    });
    if (totalCount === 0) {
      throw new NotFoundException('nothing found');
    }

    const [nodes, count] = await this.repository.findAndCount({
      where: whereCondition,
      take: limit,
      order: { id: 'DESC' } as any,
      relations: fields.relations,
      select: fields.select as any,
    });

    if (nodes.length === 0) {
      throw new NotFoundException('nothing found');
    }

    return {
      nodes,
      totalCount,
      pageInfo: {
        startCursor: (nodes[0] as any).id,
        endCursor: (nodes[nodes.length - 1] as any).id,
        hasNextPage: count > limit,
        hasPreviousPage: startCursor > 0 && totalCount > limit,
      },
    };
  }

  create(payload: object, createdBy: User) {
    const item = this.repository.create({ ...payload, createdBy } as any);
    return this.repository.save(item);
  }

  async update(id: number, payload: object) {
    const result = await this.repository.update(id, payload);
    if (result.affected === 0) {
      throw new NotFoundException('requested item was not found');
    }
    return result;
  }

  async delete(id: number) {
    const result = await this.repository.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException('requested item was not found');
    }
    return result;
  }
}
