import { DatabaseErrorInterceptor } from 'generics/databaseError.interceptor';
import { homedir } from 'os';
import { join } from 'path';

import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';

import { AppModule } from './modules/main/app.module';
import { setupSwagger } from './swagger';

declare const module: any;

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  setupSwagger(app);
  app.enableCors();
  app.useGlobalPipes(new ValidationPipe({ transform: true, whitelist: true }));
  app.useGlobalInterceptors(new DatabaseErrorInterceptor());
  app.setGlobalPrefix('api/v1');
  app.useStaticAssets(join(homedir(), 'ghazalgoo'), { prefix: '/uploads/' });
  await app.listen(4000);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}
bootstrap();
