import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import {
    Column, Entity, Index, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn
} from 'typeorm';

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { Book } from './book.entity';
import { File } from './file.entity';
import { PoetComment } from './poetComment.entity';
import { PoetLike } from './poetLike.entity';

@ObjectType()
@Entity({
  name: 'poets',
})
export class Poet {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field({ nullable: false })
  @Column({ type: 'varchar', length: 75, nullable: true })
  @ApiProperty({ nullable: false })
  @Index({ unique: true })
  slogan: string;

  @Field({ nullable: false })
  @Column({ type: 'varchar', length: 50, nullable: false })
  @ApiProperty()
  name: string;

  @Field({ nullable: true })
  @Column({ type: 'text', nullable: true })
  @ApiPropertyOptional()
  biography: string;

  @Field({ nullable: true })
  @Type(() => Date)
  @Column({ type: 'date', nullable: true })
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  birthDate: Date;

  @Field({ nullable: true })
  @Type(() => Date)
  @Column({ type: 'date', nullable: true })
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  deathDate: Date;

  @Field(() => File, { nullable: true })
  @OneToOne(() => File, { eager: true, nullable: true })
  @JoinColumn()
  @ApiProperty({ type: () => File })
  profile: File;

  @Field(() => [Book])
  @OneToMany(
    () => Book,
    book => book.poet,
    { cascade: true },
  )
  @ApiProperty({ type: () => [Book] })
  books: Book[];

  @Field(() => [PoetComment])
  @OneToMany(
    () => PoetComment,
    comment => comment.poet,
    {
      cascade: true,
    },
  )
  @ApiProperty({ type: () => [PoetComment] })
  comments: PoetComment[];

  @Field(() => [PoetLike])
  @OneToMany(
    () => PoetLike,
    like => like.poet,
    {
      cascade: true,
    },
  )
  @ApiProperty({ type: () => [PoetLike] })
  likes: PoetLike[];
}
