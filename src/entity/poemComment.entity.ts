import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import {
    Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn
} from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';

import { Poem } from './poem.entity';
import { PoemCommentLike } from './poemCommentLike.entity';
import { User } from './user.entity';

@ObjectType()
@Entity({
  name: 'poemComments',
})
export class PoemComment {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field(() => User)
  @ManyToOne(
    () => User,
    user => user.poemComments,
  )
  @ApiProperty({ type: () => User })
  createdBy: User;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field({ nullable: false })
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  comment: string;

  @Field(() => Poem)
  @ManyToOne(
    () => Poem,
    poem => poem.comments,
  )
  @ApiProperty({ type: () => Poem })
  poem: Poem;

  @Field(() => [Poem])
  @OneToMany(
    () => PoemCommentLike,
    like => like.comment,
  )
  @ApiProperty({ type: () => [PoemCommentLike] })
  likes: PoemCommentLike[];
}
