import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';

import { BookComment } from './bookComment.entity';
import { LikeAction } from './enum/likeAction.enum';
import { User } from './user.entity';

@ObjectType()
@Entity({
  name: 'bookCommentLikes',
})
export class BookCommentLike {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field(() => User)
  @ManyToOne(
    () => User,
    user => user.poemLikes,
  )
  @ApiProperty({ type: () => User })
  createdBy: User;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field(() => LikeAction)
  @Column({ type: 'enum', nullable: false, enum: LikeAction })
  @ApiProperty({ enum: [LikeAction.LIKE, LikeAction.DISLIKE] })
  action: LikeAction;

  @Field(() => BookComment)
  @ManyToOne(
    () => BookComment,
    comment => comment.likes,
  )
  @ApiProperty({ type: () => BookComment })
  comment: BookComment;
}
