import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import {
    Column, CreateDateColumn, Entity, Index, JoinColumn, ManyToOne, OneToMany, OneToOne,
    PrimaryGeneratedColumn
} from 'typeorm';

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { Book } from './book.entity';
import { BookLike } from './bookLike.entity';
import { File } from './file.entity';
import { Poem } from './poem.entity';
import { Poet } from './poet.entity';

@ObjectType()
@Entity({
  name: 'chapters',
})
export class Chapter {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field({ nullable: false })
  @Column({ type: 'varchar', length: 75, nullable: true })
  @ApiProperty({ nullable: false })
  @Index({ unique: true })
  slogan: string;

  @Field({ nullable: false })
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  name: string;

  @Field()
  @Column({ type: 'varchar', length: 255, nullable: true })
  @ApiPropertyOptional()
  description: string;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field(() => [File], { nullable: true })
  @OneToOne(() => File)
  @JoinColumn()
  @ApiProperty({ type: () => [File] })
  cover: File[];

  @Field(() => Poet)
  @ManyToOne(() => Poet)
  @ApiProperty({ type: () => Poet })
  poet: Poet;

  @Field(() => [Poem])
  @OneToMany(
    () => Poem,
    poem => poem.chapter,
    { cascade: true },
  )
  @ApiProperty({ type: () => [Poem] })
  poems: Poem[];

  @Field(() => Book)
  @ManyToOne(
    () => Book,
    book => book.chapters,
  )
  @ApiProperty({ type: () => Book })
  book: Book;

  @Field(() => [BookLike])
  @OneToMany(
    () => BookLike,
    like => like.book,
    {
      cascade: true,
    },
  )
  @ApiProperty({ type: () => [BookLike] })
  likes: BookLike[];
}
