import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';

import { Poem } from './poem.entity';
import { User } from './user.entity';

@ObjectType()
@Entity({
  name: 'poemHistory',
})
export class PoemHistory {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field(() => User)
  @ManyToOne(
    () => User,
    user => user.poemHistory,
  )
  @ApiProperty({ type: () => User })
  user: User;

  @Field(() => User)
  @ManyToOne(() => Poem)
  @ApiProperty({ type: () => Poem })
  poem: Poem;
}
