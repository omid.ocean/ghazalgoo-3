import { Type } from 'class-transformer';
import { Field, ID, Int, ObjectType } from 'type-graphql';
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

@ObjectType()
@Entity({
  name: 'files',
})
export class File {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Field({ nullable: false })
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  location: string;

  @Field({ nullable: false })
  @Column({ type: 'varchar', length: 50, nullable: false })
  @ApiProperty()
  fileName: string;

  @Field({ nullable: true })
  @Column({ type: 'varchar', length: 255, nullable: true })
  @ApiPropertyOptional()
  description: string;

  @Field({ nullable: false })
  @Column({ type: 'varchar', length: 50 })
  @ApiProperty()
  mimeType: string;

  @Field(() => Int, { nullable: false })
  @Column({ type: 'integer' })
  @ApiProperty()
  size: number;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;
}
