import { Field, ID, Int, ObjectType } from 'type-graphql';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';

import { Poem } from './poem.entity';

@ObjectType()
@Entity({
  name: 'verses',
})
export class Verse {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field(() => Int)
  @Column()
  @ApiProperty()
  order: number;

  @Field()
  @Column()
  @ApiProperty()
  isFirst: boolean;

  @Field({ nullable: false })
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  content: string;

  @Field({ nullable: true })
  @Column({ type: 'text', nullable: true })
  @ApiProperty()
  translation: string;

  @Field(() => Poem)
  @ManyToOne(
    () => Poem,
    poem => poem.verses,
  )
  @ApiProperty({ type: () => Poem })
  poem: Poem;
}
