import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import {
    Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn
} from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';

import { SuggestionStatus } from './enum/suggestionStatus.enum';
import { Poem } from './poem.entity';
import { User } from './user.entity';

@ObjectType()
@Entity({
  name: 'suggestions',
})
export class Suggestion {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Field(() => User)
  @ManyToOne(
    () => User,
    user => user.suggestions,
  )
  @ApiProperty({ type: () => User })
  createdBy: User;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field(() => Poem)
  @ManyToOne(
    () => Poem,
    poem => poem.suggestions,
  )
  @ApiProperty({ type: () => Poem })
  poem: Poem;

  @Field({ nullable: false })
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  suggestion: string;

  @Field({ nullable: false })
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  adminNotes: string;

  @Field(() => SuggestionStatus)
  @Column({ type: 'enum', nullable: false, enum: SuggestionStatus })
  @ApiProperty({
    enum: [
      SuggestionStatus.CONFIRMED,
      SuggestionStatus.PENDING_REVIEW,
      SuggestionStatus.REJECTED,
      SuggestionStatus.UNDER_REVIEW,
    ],
  })
  status: SuggestionStatus;

  @Field()
  @Type(() => Date)
  @UpdateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  updateDate: Date;
}
