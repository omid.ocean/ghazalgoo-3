import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';

import { LikeAction } from './enum/likeAction.enum';
import { PoetComment } from './poetComment.entity';
import { User } from './user.entity';

@ObjectType()
@Entity({
  name: 'poetCommentLikes',
})
export class PoetCommentLike {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field(() => User)
  @ManyToOne(
    () => User,
    user => user.poemLikes,
  )
  @ApiProperty({ type: () => User })
  createdBy: User;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field(() => LikeAction)
  @Column({ type: 'enum', nullable: false, enum: LikeAction })
  @ApiProperty({ enum: [LikeAction.LIKE, LikeAction.DISLIKE] })
  action: LikeAction;

  @Field(() => PoetComment)
  @ManyToOne(
    () => PoetComment,
    comment => comment.likes,
  )
  @ApiProperty({ type: () => PoetComment })
  comment: PoetComment;
}
