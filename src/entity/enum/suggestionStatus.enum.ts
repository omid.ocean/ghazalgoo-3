import { registerEnumType } from 'type-graphql';

enum SuggestionStatus {
  PENDING_REVIEW = 'PENDING_REVIEW',
  UNDER_REVIEW = 'UNDER_REVIEW',
  REJECTED = 'REJECTED',
  CONFIRMED = 'CONFIRMED',
}

registerEnumType(SuggestionStatus, {
  name: 'SuggestionStatus',
});

export { SuggestionStatus };
