import { registerEnumType } from 'type-graphql';

enum LikeAction {
  LIKE = 1,
  DISLIKE = -1,
}

registerEnumType(LikeAction, {
  name: 'LikeAction',
});

export { LikeAction };
