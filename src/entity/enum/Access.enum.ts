import { registerEnumType } from 'type-graphql';

enum Access {
  USER = 'USER',
  MODERATOR = 'MODERATOR',
  ADMINISTRATOR = 'ADMINISTRATOR',
  BANNED = 'BANNED',
  GUEST = 'GUEST',
  PREMIUM_USER = 'PREMIUM_USER',
}

registerEnumType(Access, {
  name: 'Access',
});

export { Access };
