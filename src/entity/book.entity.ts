import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import {
    Column, CreateDateColumn, Entity, Generated, Index, JoinColumn, ManyToOne, OneToMany, OneToOne,
    PrimaryGeneratedColumn
} from 'typeorm';

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { BookComment } from './bookComment.entity';
import { BookLike } from './bookLike.entity';
import { Chapter } from './chapter.entity';
import { File } from './file.entity';
import { Poet } from './poet.entity';

@ObjectType()
@Entity({
  name: 'books',
})
export class Book {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field({ nullable: false })
  @Column({ type: 'varchar', length: 75, nullable: true })
  @ApiProperty({ nullable: false })
  @Index({ unique: true })
  slogan: string;

  @Field({ nullable: false })
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  name: string;

  @Field({ nullable: true })
  @Column({ type: 'varchar', length: 255, nullable: true })
  @ApiPropertyOptional()
  description: string;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field(() => File, { nullable: true })
  @OneToOne(() => File, { eager: true })
  @JoinColumn()
  @ApiProperty({ type: () => File })
  cover: File;

  @Field(() => Poet)
  @ManyToOne(
    () => Poet,
    poet => poet.books,
  )
  @ApiProperty({ type: () => Poet })
  poet: Poet;

  @Field(() => [Chapter])
  @OneToMany(
    () => Chapter,
    chapter => chapter.book,
    { cascade: true },
  )
  @ApiProperty({ type: () => [Chapter] })
  chapters: Chapter[];

  @Field(() => [BookComment])
  @OneToMany(
    () => BookComment,
    comment => comment.book,
    {
      cascade: true,
    },
  )
  @ApiProperty({ type: () => [BookComment] })
  comments: BookComment[];

  @Field(() => [BookLike])
  @OneToMany(
    () => BookLike,
    like => like.book,
    {
      cascade: true,
    },
  )
  @ApiProperty({ type: () => [BookLike] })
  likes: BookLike[];
}
