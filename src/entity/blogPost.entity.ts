import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import {
    Column, CreateDateColumn, Entity, Index, ManyToOne, OneToMany, PrimaryGeneratedColumn,
    UpdateDateColumn
} from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';

import { BlogPostComment } from './blogPostComment.entity';
import { BlogPostLike } from './blogPostLike.entity';
import { User } from './user.entity';

@ObjectType()
@Entity({
  name: 'blogPosts',
})
export class BlogPost {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field({ nullable: false })
  @Column({ type: 'varchar', length: 75, nullable: true })
  @ApiProperty({ nullable: false })
  @Index({ unique: true })
  slogan: string;

  @Field(() => User)
  @ManyToOne(() => User, user => user.blogPosts)
  @ApiProperty( {type: () => User})
  createdBy: User;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field()
  @Type(() => Date)
  @UpdateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  update: Date;

  @Field({ nullable: false })
  @Column({ type: 'varchar', length: 75, nullable: false })
  @ApiProperty()
  title: string;

  @Field({ nullable: false })
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  summery: string;

  @Field({ nullable: false })
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  fullStory: string;

  @Field(() => [BlogPostLike])
  @OneToMany(() => BlogPostLike, like => like.blogPost, { cascade: true })
  @ApiProperty({ type: () => [BlogPostLike] })
  likes: BlogPostLike[];

  @Field(() => [BlogPostComment])
  @OneToMany(() => BlogPostComment, comment => comment.blogPost, {
    cascade: true,
  })
  @ApiProperty({ type: () => [BlogPostComment] })
  comments: BlogPostComment[];
}
