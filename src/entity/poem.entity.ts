import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import {
    Column, CreateDateColumn, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne,
    OneToMany, OneToOne, PrimaryGeneratedColumn
} from 'typeorm';

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { Chapter } from './chapter.entity';
import { File } from './file.entity';
import { PoemComment } from './poemComment.entity';
import { PoemLike } from './poemLike.entity';
import { PoemTag } from './poemTag.entity';
import { Suggestion } from './suggestion.entity';
import { Verse } from './verse.entity';

@ObjectType()
@Entity({
  name: 'poems',
})
export class Poem {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field({ nullable: false })
  @Column({ type: 'varchar', length: 75, nullable: true })
  @ApiProperty({ nullable: false })
  @Index({ unique: true })
  slogan: string;

  @Field({ nullable: false })
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  name: string;

  @Field({ nullable: true })
  @Column({ type: 'text', nullable: true })
  @ApiPropertyOptional()
  description: string;

  @Field(() => File, { nullable: true })
  @OneToOne(() => File, { eager: true })
  @JoinColumn()
  @ApiPropertyOptional({ type: () => File })
  cover: File;

  @Field(() => Chapter)
  @ManyToOne(
    () => Chapter,
    chapter => chapter.poems,
  )
  @ApiProperty({ type: () => Chapter })
  chapter: Chapter;

  @Field(() => [Verse])
  @OneToMany(
    () => Verse,
    verse => verse.poem,
  )
  @ApiProperty({ type: () => [Verse] })
  verses: Verse[];

  @Field(() => [PoemTag])
  @ManyToMany(
    () => PoemTag,
    poemTag => poemTag.poems,
    {
      cascade: true,
    },
  )
  @JoinTable()
  @ApiProperty({ type: () => [PoemTag] })
  tags: PoemTag[];

  @Field(() => [Suggestion])
  @OneToMany(
    () => Suggestion,
    suggestion => suggestion.poem,
    {
      cascade: true,
    },
  )
  @ApiProperty({ type: () => [Suggestion] })
  suggestions: Suggestion[];

  @Field(() => [PoemComment])
  @OneToMany(
    () => PoemComment,
    comment => comment.poem,
    {
      cascade: true,
    },
  )
  @ApiProperty({ type: () => [PoemComment] })
  comments: PoemComment[];

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field(() => [PoemLike])
  @OneToMany(
    () => PoemLike,
    like => like.poem,
    {
      cascade: true,
    },
  )
  @ApiProperty({ type: () => [PoemLike] })
  likes: PoemLike[];
}
