import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import {
    Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn
} from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';

import { BlogPost } from './blogPost.entity';
import { BlogPostCommentLike } from './blogPostCommentLike.entity';
import { User } from './user.entity';

@ObjectType()
@Entity({
  name: 'blogPostComments',
})
export class BlogPostComment {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field(() => User)
  @ManyToOne(() => User)
  @ApiProperty({ type: () => User })
  createdBy: User;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field({ nullable: false })
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  comment: string;

  @Field(() => BlogPost)
  @ManyToOne(
    () => BlogPost,
    blogPost => blogPost.comments,
  )
  @ApiProperty({ type: () => BlogPost })
  blogPost: BlogPost;

  @Field(() => [BlogPostCommentLike])
  @OneToMany(
    () => BlogPostCommentLike,
    like => like.comment,
    {
      cascade: true,
    },
  )
  @ApiProperty({ type: () => [BlogPostCommentLike] })
  likes: BlogPostCommentLike[];
}
