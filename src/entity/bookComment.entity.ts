import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import {
    Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn
} from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';

import { Book } from './book.entity';
import { BookCommentLike } from './bookCommentLike.entity';
import { User } from './user.entity';

@ObjectType()
@Entity({
  name: 'bookComments',
})
export class BookComment {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field(() => User)
  @ManyToOne(
    () => User,
    user => user.bookComments,
  )
  @ApiProperty({ type: () => User })
  createdBy: User;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field()
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  comment: string;

  @Field(() => Book)
  @ManyToOne(
    () => Book,
    book => book.comments,
  )
  @ApiProperty({ type: () => Book })
  book: Book;

  @Field(() => [BookCommentLike])
  @OneToMany(
    () => BookCommentLike,
    like => like.comment,
  )
  @ApiProperty({ type: () => [BookCommentLike] })
  likes: BookCommentLike[];
}
