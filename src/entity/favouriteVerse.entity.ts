import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import { CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';

import { User } from './user.entity';
import { Verse } from './verse.entity';

@ObjectType()
@Entity({
  name: 'favouriteVerses',
})
export class FavouriteVerse {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field(() => User)
  @ManyToOne(
    () => User,
    user => user.favouriteVerses,
  )
  @ApiProperty({ type: () => User })
  user: User;

  @Field(() => Verse)
  @ManyToOne(() => Verse)
  @ApiProperty({ type: () => Verse })
  verse: Verse;
}
