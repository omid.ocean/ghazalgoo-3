import { compare } from 'bcrypt';
import { Exclude, Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import {
    Column, CreateDateColumn, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn,
    Unique
} from 'typeorm';

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { BlogPost } from './blogPost.entity';
import { BlogPostLike } from './blogPostLike.entity';
import { BookComment } from './bookComment.entity';
import { BookLike } from './bookLike.entity';
import { Access } from './enum/Access.enum';
import { FavouritePoem } from './favouritePoem.entity';
import { FavouriteVerse } from './favouriteVerse.entity';
import { File } from './file.entity';
import { PoemComment } from './poemComment.entity';
import { PoemHistory } from './poemHistory.entity';
import { PoemLike } from './poemLike.entity';
import { PoetComment } from './poetComment.entity';
import { PoetLike } from './poetLike.entity';
import { Suggestion } from './suggestion.entity';

@ObjectType()
@Entity({
  name: 'users',
})
@Unique(['username', 'telegramId'])
export class User {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Field()
  @Column({ type: 'varchar', length: 50, nullable: false })
  @ApiProperty()
  firstName: string;

  @Field()
  @Column({ type: 'varchar', length: 50, nullable: false })
  @ApiProperty()
  lastName: string;

  @Field(() => File, { nullable: true })
  @OneToOne(() => File, { eager: true })
  @JoinColumn()
  @ApiPropertyOptional()
  profile: File;

  @Field({ nullable: true })
  @Column({ type: 'varchar', length: 15, unique: true, nullable: true })
  @ApiPropertyOptional()
  username: string;

  @Field()
  @Column({ type: 'varchar', unique: true })
  @ApiProperty()
  email: string;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  joinDate: Date;

  @Column()
  @Exclude()
  password: string;

  @Field({ nullable: true })
  @Column({ type: 'varchar', length: 15, nullable: true })
  telegramId: string;

  @Field(() => Access)
  @Column({ type: 'enum', enum: Access, default: Access.USER })
  @ApiProperty({
    enum: [
      Access.ADMINISTRATOR,
      Access.BANNED,
      Access.GUEST,
      Access.MODERATOR,
      Access.PREMIUM_USER,
      Access.USER,
    ],
  })
  access: Access;

  @Field(() => [PoemHistory])
  @OneToMany(
    () => PoemHistory,
    poemHistory => poemHistory.user,
  )
  @ApiProperty({ type: () => [PoemHistory] })
  poemHistory: PoemHistory[];

  @Field(() => [FavouritePoem])
  @OneToMany(
    () => FavouritePoem,
    favPoem => favPoem.user,
  )
  @ApiProperty({ type: () => [FavouritePoem] })
  favouritePoems: FavouritePoem[];

  @Field(() => [FavouriteVerse])
  @OneToMany(
    () => FavouriteVerse,
    favVerse => favVerse.user,
  )
  @ApiProperty({ type: () => [FavouriteVerse] })
  favouriteVerses: FavouriteVerse[];

  @Field(() => [Suggestion])
  @OneToMany(
    () => Suggestion,
    suggestion => suggestion.createdBy,
  )
  @ApiProperty({ type: () => [Suggestion] })
  suggestions: Suggestion[];

  @Field(() => [PoemComment])
  @OneToMany(
    () => PoemComment,
    comment => comment.createdBy,
  )
  @ApiProperty({ type: () => [PoemComment] })
  poemComments: PoemComment[];

  @Field(() => [PoemLike])
  @OneToMany(
    () => PoemLike,
    like => like.createdBy,
  )
  @ApiProperty({ type: () => [PoemLike] })
  poemLikes: PoemLike[];

  @Field(() => [PoetComment])
  @OneToMany(
    () => PoetComment,
    comment => comment.createdBy,
  )
  @ApiProperty({ type: () => [PoetComment] })
  poetComments: PoetComment[];

  @Field(() => [PoetLike])
  @OneToMany(
    () => PoetLike,
    like => like.createdBy,
  )
  @ApiProperty({ type: [PoetLike] })
  poetLikes: PoetLike[];

  @Field(() => [BookComment])
  @OneToMany(
    () => BookComment,
    comment => comment.createdBy,
  )
  @ApiProperty({ type: () => [BookComment] })
  bookComments: BookComment[];

  @Field(() => [BookLike])
  @OneToMany(
    () => BookLike,
    like => like.createdBy,
  )
  @ApiProperty({ type: () => [BookLike] })
  bookLikes: BookLike[];

  @Field(() => [BlogPost])
  @OneToMany(
    () => BlogPost,
    blogPost => blogPost.createdBy,
  )
  @ApiProperty({ type: () => [BlogPost] })
  blogPosts: BlogPost[];

  @Field(() => [BlogPostLike])
  @OneToMany(
    () => BlogPostLike,
    blogPostLike => blogPostLike.createdBy,
  )
  @ApiProperty({ type: () => [BlogPostLike] })
  blogPostLikes: BlogPostLike[];

  async validatePassword(password: string): Promise<boolean> {
    return await compare(password, this.password);
  }
}
