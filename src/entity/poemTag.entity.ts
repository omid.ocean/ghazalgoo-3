import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import { Column, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';

import { Poem } from './poem.entity';

@ObjectType()
@Entity({
  name: 'poemTags',
})
export class PoemTag {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field()
  @Type(() => Date)
  @Column({ type: 'varchar', length: 25, nullable: false })
  @ApiProperty()
  content: string;

  @Field(() => [Poem])
  @ManyToMany(
    () => Poem,
    poem => poem.tags,
  )
  @ApiProperty({ type: () => [Poem] })
  poems: Poem[];
}
