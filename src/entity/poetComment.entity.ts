import { Type } from 'class-transformer';
import { Field, ID, ObjectType } from 'type-graphql';
import {
    Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn
} from 'typeorm';

import { ApiProperty } from '@nestjs/swagger';

import { Poet } from './poet.entity';
import { PoetCommentLike } from './poetCommentLike.entity';
import { User } from './user.entity';

@ObjectType()
@Entity({
  name: 'poetComments',
})
export class PoetComment {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Field(() => User)
  @ManyToOne(
    () => User,
    user => user.poetComments,
  )
  @ApiProperty({ type: () => User })
  createdBy: User;

  @Field()
  @Type(() => Date)
  @CreateDateColumn()
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  creationDate: Date;

  @Field({ nullable: false })
  @Column({ type: 'text', nullable: false })
  @ApiProperty()
  comment: string;

  @Field(() => Poet)
  @ManyToOne(
    () => Poet,
    poet => poet.comments,
  )
  @ApiProperty({ type: () => Poet })
  poet: Poet;

  @Field(() => [PoetCommentLike])
  @OneToMany(
    () => PoetCommentLike,
    like => like.comment,
  )
  @ApiProperty({ type: () => [PoetCommentLike] })
  likes: PoetCommentLike[];
}
