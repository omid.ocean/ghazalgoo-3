export const SWAGGER_API_ROOT = 'api/docs';
export const SWAGGER_API_NAME = 'Ghazalgoo 2.0 Api';
export const SWAGGER_API_DESCRIPTION = 'Ghazalgoo rest api documentation';
export const SWAGGER_API_CURRENT_VERSION = '0.1';
export const SWAGGER_API_AUTH_NAME = 'Authorization';
export const SWAGGER_API_AUTH_LOCATION = 'header';
export const SWAGGER_API_SERVER = '/api/v1';
