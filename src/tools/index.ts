import { IAddAttr } from './IAddAttr.interface';

/**
 * turns structured graphql data to typeorm find options
 * @param object structured graphql query
 * @returns typeorm find options
 */
export const addAttributeInput = (object: object) => {
  const response: IAddAttr = {
    select: [],
    relations: [],
  };
  for (const field in object) {
    if (typeof object[field] === 'object') {
      if (!isEmpty(object[field])) {
        const processed = processAddAttr(object[field], field);
        response.relations.push(field, ...processed);
      } else {
        response.select.push(field);
      }
    }
  }
  response.select.indexOf('id') === -1 ? response.select.push('id') : undefined;
  return response;
};

const processAddAttr = (object: object, parent: string) => {
  const response: string[] = [];
  for (const field in object) {
    if (typeof object[field] === 'object') {
      const resElement = parent + '.' + field;
      if (!isEmpty(object[field])) {
        const processed = processAddAttr(object[field], resElement);
        response.push(resElement, ...processed);
      }
    }
  }
  return response;
};

export const isEmpty = (obj: object) => {
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
};
