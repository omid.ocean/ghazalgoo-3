import { createParamDecorator } from '@nestjs/common';

import { User } from '../entity/user.entity';

export const GetUser = createParamDecorator(
  (_data, [_root, _args, ctx, _info]): User => ctx.req.user,
);

export const GetUserRest = createParamDecorator(
  (data, req): User => {
    return req.user;
  },
);
