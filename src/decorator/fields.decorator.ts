import * as graphqlFields from 'graphql-fields';
import { addAttributeInput } from 'tools';

import { createParamDecorator } from '@nestjs/common';

/**
 * NestJS decorator for filtering graphql`s requested fields and returning a typeorm find options object
 */
export const GetFields = createParamDecorator(
  (_data, [_root, _args, _ctx, info]) => {
    let fields = graphqlFields(info, {}, { processArguments: true });
    if (fields.nodes) {
      fields = fields.nodes;
    }
    return addAttributeInput(fields);
  },
);
