import { Access } from 'entity/enum/Access.enum';

import { SetMetadata } from '@nestjs/common';

export const Roles = (...access: Access[]) => SetMetadata('access', access);
