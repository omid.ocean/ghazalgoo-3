import { Poet } from 'entity/poet.entity';
import { RepoBase } from 'generics/baseRepository.repository';
import { IAddAttr } from 'tools/IAddAttr.interface';
import { EntityRepository } from 'typeorm';

@EntityRepository(Poet)
export class PoetRepository extends RepoBase<Poet> {
  getAllPaginated(
    startCursor: number,
    endCursor: number,
    limit: number,
    search: string,
    fields: IAddAttr,
  ) {
    return super.abstractGetAllPaginated(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
      'name',
    );
  }
}
