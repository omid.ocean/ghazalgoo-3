import { Poet } from 'entity/poet.entity';
import PaginatedResponse from 'generics/pagination.dto';
import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class PaginatedPoetResponse extends PaginatedResponse(Poet) {}
