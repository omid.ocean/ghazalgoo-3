import { Poet } from 'entity/poet.entity';

import { Controller, Get, Param, ParseIntPipe, Query } from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';

import { PoetService } from './poet.service';

@Controller('poet')
@ApiTags('poet')
export class PoetController {
  constructor(private readonly service: PoetService) {}

  @Get()
  @ApiQuery({ name: 'limit', required: false, example: 10 })
  @ApiQuery({ name: 'start', required: false, example: 1 })
  @ApiQuery({ name: 'end', required: false, example: 100 })
  @ApiQuery({ name: 'search', required: false, example: 'سعدی' })
  async getPoets(
    @Query('limit', new ParseIntPipe()) limit: number = 10,
    @Query('start') start?: number,
    @Query('end') end?: number,
    @Query('search') search: string = '',
  ) {
    return this.service.getPoets(start, end, limit, search, {
      relations: ['profile'],
      select: ['id', 'slogan', 'name'],
    });
  }

  @Get(':slug')
  @ApiResponse({
    status: 200,
    description: 'Poet found',
    type: Poet,
  })
  async getPoet(@Param('slug') slug: string) {
    return this.service.findBySlug(slug);
  }
}
