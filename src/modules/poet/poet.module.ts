import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { PoetController } from './poet.controller';
import { PoetRepository } from './poet.repository';
import { PoetResolver } from './poet.resolver';
import { PoetService } from './poet.service';

@Module({
  controllers: [PoetController],
  providers: [PoetService, PoetResolver],
  imports: [TypeOrmModule.forFeature([PoetRepository])],
})
export class PoetModule {}
