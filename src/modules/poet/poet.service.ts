import { IAddAttr } from 'tools/IAddAttr.interface';

import { Injectable, Logger } from '@nestjs/common';

import { PaginatedPoetResponse } from './dto/pagination.dto';
import { PoetRepository } from './poet.repository';

@Injectable()
export class PoetService {
  private readonly logger = new Logger(PoetService.name);

  constructor(private readonly repository: PoetRepository) {}

  getPoetByID(id: number, fields: IAddAttr) {
    return this.repository.findById(id, fields);
  }

  findBySlug(slogan: string) {
    return this.repository.findBySlug({
      where: { slogan },
      relations: ['books', 'books.cover'],
    });
  }

  async getPoets(
    startCursor: number,
    endCursor: number,
    limit: number,
    search: string,
    fields: IAddAttr,
  ): Promise<PaginatedPoetResponse> {
    return await this.repository.getAllPaginated(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
    );
  }
}
