import { Poet } from 'entity/poet.entity';
import { IAddAttr } from 'tools/IAddAttr.interface';
import { Int } from 'type-graphql';

import { Args, Query, Resolver } from '@nestjs/graphql';

import { GetFields } from '../../decorator/fields.decorator';
import { PaginatedPoetResponse } from './dto/pagination.dto';
import { PoetService } from './poet.service';

@Resolver(() => Poet)
export class PoetResolver {
  constructor(private readonly poetService: PoetService) {}

  @Query(() => Poet)
  async getPoet(
    @Args({ name: 'id', type: () => Int }) id: number,
    @GetFields() fields: IAddAttr,
  ) {
    return this.poetService.getPoetByID(id, fields);
  }

  @Query(() => PaginatedPoetResponse)
  async getPoets(
    @Args({
      name: 'startCursor',
      type: () => Int,
      defaultValue: 0,
      description: 'start Cursor',
    })
    startCursor: number,
    @Args({
      name: 'endCursor',
      type: () => Int,
      nullable: true,
      description: 'end Cursor',
    })
    endCursor: number,
    @Args({
      name: 'limit',
      type: () => Int,
      nullable: true,
      defaultValue: 10,
      description: 'limits the amount of displayed items',
    })
    limit: number,
    @Args({
      name: 'search',
      nullable: true,
      defaultValue: '',
      description: 'optional search string used for searching poets names',
      type: () => String,
    })
    search: string,
    @GetFields() fields: IAddAttr,
  ) {
    const poet = await this.poetService.getPoets(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
    );
    return poet;
  }
}
