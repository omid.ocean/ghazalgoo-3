import { BlogPost } from 'entity/blogPost.entity';
import { BlogPostComment } from 'entity/blogPostComment.entity';
import { LikeAction } from 'entity/enum/likeAction.enum';
import { User } from 'entity/user.entity';
import { RepoBase } from 'generics/baseRepository.repository';
import { IUserIntractable } from 'generics/userIntractable.interface';
import { IAddAttr } from 'tools/IAddAttr.interface';
import { EntityRepository } from 'typeorm';

import { BadRequestException } from '@nestjs/common';

import { BlogPostCommentLike } from '../../entity/blogPostCommentLike.entity';
import { BlogPostLike } from '../../entity/blogPostLike.entity';

@EntityRepository(BlogPost)
export class BlogPostRepository extends RepoBase<BlogPost>
  implements IUserIntractable {
  async addComment(id: number, user: User, commentText: string) {
    const post = await this.repository.findOne(id);
    if (!post) {
      throw new BadRequestException('invalid blogPost id!');
    }
    const comment = new BlogPostComment();
    comment.comment = commentText;
    comment.createdBy = user;
    comment.blogPost = post;
    return this.manager.save(comment);
  }

  async likeComment(id: number, user: User, action: LikeAction) {
    const comment = await this.manager.findOne(BlogPostComment, id);
    if (!comment) {
      throw new BadRequestException('invalid id!');
    }
    const existingLike = await this.manager.findOne(BlogPostCommentLike, {
      where: { comment, createdBy: user },
    });
    if (!existingLike) {
      const like = new BlogPostCommentLike();
      like.createdBy = user;
      like.comment = comment;
      like.action = action;
      return this.manager.save(like);
    }
    existingLike.action = action;
    return this.manager.save(existingLike);
  }

  async like(id: number, user: User, action: LikeAction) {
    const blogPost = await this.repository.findOne(id);
    if (!blogPost) {
      throw new BadRequestException('invalid blogPost id!');
    }
    const existingLike = await this.manager.findOne(BlogPostLike, {
      where: { blogPost, createdBy: user },
    });
    if (!existingLike) {
      const like = new BlogPostLike();
      like.createdBy = user;
      like.blogPost = blogPost;
      like.action = action;
      return this.manager.save(like);
    }
    existingLike.action = action;
    return this.manager.save(existingLike);
  }

  getAllPaginated(
    startCursor: number,
    endCursor: number,
    limit: number,
    search: string,
    fields: IAddAttr,
  ) {
    return super.abstractGetAllPaginated(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
      'title',
    );
  }
}
