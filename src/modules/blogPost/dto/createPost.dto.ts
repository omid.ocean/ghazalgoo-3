import { IsString, MaxLength, MinLength } from 'class-validator';
import { Field, InputType } from 'type-graphql';

import { ApiProperty } from '@nestjs/swagger';

@InputType()
export class BlogPostDTO {
  @Field({ nullable: false })
  @ApiProperty()
  @IsString()
  @MinLength(3)
  @MaxLength(75)
  title: string;

  @Field({ nullable: false })
  @ApiProperty()
  @MinLength(25)
  @IsString()
  summery: string;

  @Field({ nullable: false })
  @ApiProperty()
  @MinLength(25)
  @IsString()
  fullStory: string;

  @Field({ nullable: false })
  @ApiProperty()
  @MaxLength(70)
  @IsString()
  slogan: string;
}
