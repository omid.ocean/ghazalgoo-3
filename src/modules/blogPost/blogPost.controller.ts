import { BlogPost } from 'entity/blogPost.entity';

import { Controller, Get, Param } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { BlogPostService } from './blogPost.service';

@Controller('blog')
@ApiTags('blog')
export class BlogPostController {
  constructor(private readonly service: BlogPostService) {}

  @Get(':slug')
  @ApiResponse({
    status: 201,
    description: 'Successful Login',
    type: BlogPost,
  })
  async getBlogPost(@Param('slug') slug: string) {
    return this.service.findBySlug(slug);
  }
}
