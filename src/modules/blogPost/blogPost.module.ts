import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BlogPostController } from './blogPost.controller';
import { BlogPostRepository } from './blogPost.repository';
import { BookResolver } from './blogPost.resolver';
import { BlogPostService } from './blogPost.service';

@Module({
  providers: [BlogPostService, BookResolver],
  imports: [TypeOrmModule.forFeature([BlogPostRepository])],
  controllers: [BlogPostController],
})
export class BlogPostModule {}
