import { GetFields } from 'decorator/fields.decorator';
import { Roles } from 'decorator/roles.decorator';
import { GetUser } from 'decorator/user.decorator';
import { BlogPost } from 'entity/blogPost.entity';
import { BlogPostComment } from 'entity/blogPostComment.entity';
import { Access } from 'entity/enum/Access.enum';
import { LikeAction } from 'entity/enum/likeAction.enum';
import { User } from 'entity/user.entity';
import { GqlAuthGuard } from 'modules/auth/auth.guard';
import { RolesGuard } from 'modules/auth/roles.guard';
import { IAddAttr } from 'tools/IAddAttr.interface';
import { ID, Int } from 'type-graphql';

import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { BlogPostCommentLike } from '../../entity/blogPostCommentLike.entity';
import { BlogPostLike } from '../../entity/blogPostLike.entity';
import { UpdateAndDelete } from '../../generics/crud.dto';
import { BlogPostService } from './blogPost.service';
import { BlogPostDTO } from './dto/createPost.dto';
import { PaginatedBlogPostResponse } from './dto/pagination.dto';

@Resolver(() => BlogPost)
export class BookResolver {
  constructor(private readonly service: BlogPostService) {}

  @Query(() => BlogPost)
  async getBlogPost(
    @Args({ name: 'id', type: () => Int }) id: number,
    @GetFields() fields: IAddAttr,
  ) {
    return this.service.getBlogPostById(id, fields);
  }

  @Query(() => PaginatedBlogPostResponse)
  async getBlogPosts(
    @Args({
      name: 'startCursor',
      type: () => Int,
      defaultValue: 0,
      description: 'start Cursor',
    })
    startCursor: number,
    @Args({
      name: 'endCursor',
      type: () => Int,
      nullable: true,
      description: 'end Cursor',
    })
    endCursor: number,
    @Args({
      name: 'limit',
      type: () => Int,
      nullable: true,
      defaultValue: 10,
      description: 'limits the amount of displayed items',
    })
    limit: number,
    @Args({
      name: 'search',
      nullable: true,
      defaultValue: '',
      description: 'optional search string used for searching blogPost names',
      type: () => String,
    })
    search: string,
    @GetFields() fields: IAddAttr,
  ) {
    const poet = await this.service.getBlogPosts(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
    );
    return poet;
  }

  @Mutation(() => BlogPostComment)
  @UseGuards(GqlAuthGuard)
  async addBlogPostComment(
    @GetUser() user: User,
    @Args({ name: 'id', type: () => ID }) id: number,
    @Args({
      name: 'comment',
      nullable: false,
      type: () => String,
    })
    comment: string,
  ) {
    return this.service.addComment(id, user, comment);
  }

  @Mutation(() => BlogPostCommentLike)
  @UseGuards(GqlAuthGuard)
  async likeBlogPostComment(
    @GetUser() user: User,
    @Args({ name: 'id', type: () => ID }) id: number,
    @Args({
      name: 'action',
      nullable: false,
      type: () => LikeAction,
    })
    action: LikeAction,
  ) {
    return this.service.likeComment(id, user, action);
  }

  @Mutation(() => BlogPostLike)
  @UseGuards(GqlAuthGuard)
  async likeBlogPost(
    @GetUser() user: User,
    @Args({ name: 'id', type: () => ID }) id: number,
    @Args({
      name: 'action',
      nullable: false,
      type: () => LikeAction,
    })
    action: LikeAction,
  ) {
    return this.service.like(id, user, action);
  }

  @Mutation(() => BlogPost)
  @UseGuards(GqlAuthGuard)
  async createBlogPost(
    @GetUser() user: User,
    @Args('input') payload: BlogPostDTO,
  ) {
    return await this.service.createBlogPost(payload, user);
  }

  @Mutation(() => UpdateAndDelete)
  @UseGuards(GqlAuthGuard)
  async editBlogPost(
    @Args('input') payload: BlogPostDTO,
    @Args({ name: 'id', type: () => ID }) id: number,
  ) {
    return await this.service.editBlogPost(id, payload);
  }

  @Mutation(() => UpdateAndDelete)
  @UseGuards(GqlAuthGuard, RolesGuard)
  @Roles(Access.ADMINISTRATOR)
  async deleteBlogPost(@Args({ name: 'id', type: () => ID }) id: number) {
    return this.service.deleteBlogPost(id);
  }
}
