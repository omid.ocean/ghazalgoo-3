import { LikeAction } from 'entity/enum/likeAction.enum';
import { User } from 'entity/user.entity';
import { IUserIntractable } from 'generics/userIntractable.interface';
import { IAddAttr } from 'tools/IAddAttr.interface';

import { Injectable, Logger } from '@nestjs/common';

import { BlogPostRepository } from './blogPost.repository';
import { BlogPostDTO } from './dto/createPost.dto';
import { PaginatedBlogPostResponse } from './dto/pagination.dto';

@Injectable()
export class BlogPostService implements IUserIntractable {
  private readonly logger = new Logger(BlogPostService.name);

  constructor(private readonly repository: BlogPostRepository) {}

  getBlogPostById(id: number, fields: IAddAttr) {
    return this.repository.findById(id, fields);
  }

  findBySlug(slogan: string) {
    return this.repository.findBySlug({ where: { slogan } });
  }

  async getBlogPosts(
    startCursor: number,
    endCursor: number,
    limit: number,
    search: string,
    fields: IAddAttr,
  ): Promise<PaginatedBlogPostResponse> {
    return await this.repository.getAllPaginated(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
    );
  }

  deleteBlogPost(id: number) {
    return this.repository.delete(id);
  }

  editBlogPost(id: number, payload: BlogPostDTO) {
    return this.repository.update(id, payload);
  }

  createBlogPost(payload: BlogPostDTO, user: User) {
    return this.repository.create(payload, user);
  }

  addComment(id: number, user: User, comment: string) {
    return this.repository.addComment(id, user, comment);
  }

  like(id: number, user: User, action: LikeAction) {
    return this.repository.like(id, user, action);
  }

  likeComment(id: number, user: User, action: LikeAction) {
    return this.repository.likeComment(id, user, action);
  }
}
