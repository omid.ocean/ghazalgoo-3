import { File } from 'entity/file.entity';
import { Int } from 'type-graphql';

import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { FileService } from './file.service';

@Resolver(() => File)
export class FileResolver {
  constructor(private readonly fileService: FileService) {}

  @Query(() => File)
  async getFile(@Args({ name: 'id', type: () => Int }) id: number) {
    return this.fileService.getFileById(id);
  }
}
