import { Roles } from 'decorator/roles.decorator';
import { Access } from 'entity/enum/Access.enum';
import { File } from 'entity/file.entity';
import { ErrorDTO } from 'generics/error.dto';
import { RolesGuard } from 'modules/auth/roles.guard';

import {
    Body, Controller, Get, Param, Post, Res, UploadedFile, UseGuards, UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import {
    ApiBadRequestResponse, ApiBearerAuth, ApiConsumes, ApiCreatedResponse, ApiForbiddenResponse,
    ApiNotFoundResponse, ApiOkResponse, ApiTags
} from '@nestjs/swagger';

import { CreateFile } from './dto/createFile.dto';
import { FileService } from './file.service';
import { MulterFile } from './uploadedFile.interface';

@Controller('files')
@ApiTags('File')
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @Post()
  @ApiBearerAuth()
  @UseInterceptors(FileInterceptor('file'))
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiConsumes('multipart/form-data')
  @Roles(Access.ADMINISTRATOR, Access.USER)
  @ApiCreatedResponse({
    description: 'file has been successfully uploaded.',
    type: File,
  })
  @ApiForbiddenResponse({
    description: 'you must have sufficient access.',
    type: ErrorDTO,
  })
  uploadFile(
    @UploadedFile() file: MulterFile,
    @Body() createCatDto: CreateFile,
  ) {
    return this.fileService.handleUploadedFile(file, createCatDto);
  }

  @Get(':imgId')
  @ApiOkResponse({
    description: 'file was found.',
    type: File,
  })
  @ApiBadRequestResponse({
    description: 'invalid imgID.',
    type: ErrorDTO,
  })
  @ApiNotFoundResponse({
    description: 'requested file was not found.',
    type: ErrorDTO,
  })
  async getFile(@Param('imgId') imgId: number) {
    return this.fileService.getFileById(imgId);
  }
}
