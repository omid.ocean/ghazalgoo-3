import { ConfigModule, ConfigService } from 'modules/config';
import { diskStorage } from 'multer';
import { homedir } from 'os';
import { extname, join } from 'path';
import * as uuidv1 from 'uuid/v1';

import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FileController } from './file.controller';
import { FileRepository } from './file.repository';
import { FileResolver } from './file.resolver';
import { FileService } from './file.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([FileRepository]),
    ConfigModule,
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        storage: diskStorage({
          destination: (_req, _file, cb) => {
            cb(null, join(homedir(), configService.get('UPLOAD_PATH')));
          },
          filename: (_req, file, cb) => {
            cb(null, uuidv1() + extname(file.originalname));
          },
        }),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [FileController],
  providers: [FileService, FileResolver],
})
export class FileModule {}
