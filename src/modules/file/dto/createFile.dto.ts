import { IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';
import { Field, InputType } from 'type-graphql';

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

@InputType()
export class CreateFile {
  @Field({ nullable: false })
  @IsString()
  @IsNotEmpty()
  @MaxLength(50)
  @ApiProperty()
  fileName: string;

  @Field({ nullable: true })
  @IsOptional()
  @IsString()
  @MaxLength(255)
  @ApiPropertyOptional()
  description: string;
}
