import { File } from 'entity/file.entity';
import { EntityRepository, Repository } from 'typeorm';

import { InternalServerErrorException } from '@nestjs/common';

import { CreateFile } from './dto/createFile.dto';
import { MulterFile } from './uploadedFile.interface';

@EntityRepository(File)
export class FileRepository extends Repository<File> {
  get(id: number) {
    return this.findOne(id);
  }

  newFile(uploadedFile: MulterFile, payload: CreateFile) {
    try {
      const file = new File();
      file.description = payload.description;
      file.fileName = payload.fileName;
      file.location = uploadedFile.filename;
      file.mimeType = uploadedFile.mimetype;
      file.size = uploadedFile.size;
      return this.save(file);
    } catch (e) {
      throw new InternalServerErrorException('error saving file to database ');
    }
  }
}
