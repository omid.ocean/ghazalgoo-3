import { homedir } from 'os';
import { join } from 'path';

import { Injectable } from '@nestjs/common';

import { ConfigService } from '../config/config.service';
import { CreateFile } from './dto/createFile.dto';
import { FileRepository } from './file.repository';
import { MulterFile } from './uploadedFile.interface';

@Injectable()
export class FileService {
  constructor(
    private readonly fileRepository: FileRepository,
    private readonly config: ConfigService,
  ) {}

  getFileById(id: number) {
    return this.fileRepository.get(id);
  }

  async handleUploadedFile(file: MulterFile, payload: CreateFile) {
    return this.fileRepository.newFile(file, payload);
  }

  async getImage(id: number) {
    const file = await this.getFileById(id);
    const path = join(homedir(), this.config.get('UPLOAD_PATH'), file.location);
    return path;
  }
}
