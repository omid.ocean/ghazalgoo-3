import { AuthModule } from 'modules/auth';
import { BlogPostModule } from 'modules/blogPost';
import { BookModule } from 'modules/book';
import { ChapterModule } from 'modules/chapter';
import { ConfigModule, ConfigService } from 'modules/config';
import { FileModule } from 'modules/file';
import { PoemModule } from 'modules/poem';
import { PoetModule } from 'modules/poet';
import { getMetadataArgsStorage } from 'typeorm';

import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule, TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
      playground: true,
      context: ({ req }) => ({ req }),
    }),
    ConfigModule,
    AuthModule,
    FileModule,
    PoetModule,
    BookModule,
    ChapterModule,
    PoemModule,
    BlogPostModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          type: configService.get('DB_TYPE'),
          host: configService.get('DB_HOST'),
          port: configService.get('DB_PORT'),
          username: configService.get('DB_USERNAME'),
          password: configService.get('DB_PASSWORD'),
          database: configService.get('DB_DATABASE'),
          entities: getMetadataArgsStorage().tables.map(tbl => tbl.target),
          synchronize: configService.isEnv('dev'),
          keepConnectionAlive: true,
        } as TypeOrmModuleAsyncOptions;
      },
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
