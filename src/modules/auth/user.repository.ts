import * as bcrypt from 'bcrypt';
import { User } from 'entity/user.entity';
import { EntityRepository, Repository } from 'typeorm';

import {
    InternalServerErrorException, NotAcceptableException, UnauthorizedException
} from '@nestjs/common';

import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async get(id: number) {
    return this.findOne(id);
  }

  async getByUsername(username: string) {
    return await this.createQueryBuilder('users')
      .where('users.username = :username')
      .setParameter('username', username)
      .getOne();
  }

  async getByEmail(email: string) {
    return await this.createQueryBuilder('users')
      .where('users.email = :email')
      .setParameter('email', email)
      .getOne();
  }

  async createUser(payload: RegisterDto) {
    try {
      const salt = await bcrypt.genSalt();
      const user = new User();
      user.firstName = payload.firstName;
      user.lastName = payload.lastName;
      user.password = await this.hashPassword(payload.password, salt);
      user.username = payload.username;
      user.email = payload.email;
      return await this.save(user);
    } catch (e) {
      if (e.code === '23505') {
        throw new NotAcceptableException(
          'User with provided username/email already created.',
        );
      }
      throw new InternalServerErrorException();
    }
  }

  async validateUserPassword(loginDto: LoginDto): Promise<User> {
    const user = await this.getByEmail(loginDto.email);
    if (user && (await user.validatePassword(loginDto.password))) {
      return user;
    } else {
      throw new UnauthorizedException();
    }
  }

  private async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }
}
