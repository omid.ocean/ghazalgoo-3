import { User } from 'entity/user.entity';
import { Field, ObjectType } from 'type-graphql';

import { ApiProperty } from '@nestjs/swagger';

@ObjectType()
export class AuthResponse {
  @Field()
  @ApiProperty()
  expiresIn: string;
  @Field()
  @ApiProperty()
  accessToken: string;
  @Field(() => User)
  @ApiProperty({ type: () => User })
  user: User;
}
