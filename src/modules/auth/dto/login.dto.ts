import { IsEmail, IsString, MaxLength, MinLength } from 'class-validator';
import { Field, InputType } from 'type-graphql';

import { ApiProperty } from '@nestjs/swagger';

@InputType()
export class LoginDto {
  @Field()
  @IsEmail()
  @ApiProperty()
  email: string;

  @Field()
  @IsString()
  @MinLength(8)
  @ApiProperty()
  password: string;
}
