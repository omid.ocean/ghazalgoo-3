import { IsEmail, IsOptional, IsString, Matches, MaxLength, MinLength } from 'class-validator';
import { Field, InputType } from 'type-graphql';

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

@InputType()
export class RegisterDto {
  @Field({ nullable: true })
  @IsString()
  @IsOptional()
  @MinLength(4)
  @MaxLength(15)
  @ApiPropertyOptional()
  username: string;

  @Field()
  @IsEmail()
  @ApiProperty()
  email: string;

  @Field()
  @IsString()
  @MinLength(3)
  @MaxLength(50)
  @ApiProperty()
  firstName: string;

  @Field()
  @IsString()
  @MinLength(3)
  @MaxLength(50)
  @ApiProperty()
  lastName: string;

  @Field()
  @IsString()
  @MinLength(8)
  // @Matches(/(?=.*[A-Z].*[A-Z])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z]).*$/, {
  //   message: 'password is too weak',
  // })
  @ApiProperty()
  password: string;
}
