import { User } from 'entity/user.entity';

import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { ConfigService } from '../config';
import { AuthResponse } from './dto/auth-response.dto';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import { UserRepository } from './user.repository';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    private readonly userRepository: UserRepository,
  ) {}

  async register(payload: RegisterDto): Promise<AuthResponse> {
    const user = await this.userRepository.createUser(payload);
    return await this.createToken(user);
  }

  async login(payload: LoginDto): Promise<AuthResponse> {
    const user = await this.userRepository.validateUserPassword(payload);
    return await this.createToken(user);
  }

  private async createToken(user: User): Promise<AuthResponse> {
    delete user.password;
    return {
      expiresIn: this.configService.get('JWT_EXPIRATION_TIME'),
      accessToken: await this.jwtService.sign({ id: user.id }),
      user,
    };
  }
}
