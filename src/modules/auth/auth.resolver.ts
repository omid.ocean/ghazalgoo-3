import { GetFields } from 'decorator/fields.decorator';
import { GetUser } from 'decorator/user.decorator';
import { User } from 'entity/user.entity';

import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { GqlAuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { AuthResponse } from './dto/auth-response.dto';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';

@Resolver(() => User)
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Query(() => AuthResponse)
  async login(@Args('input') payload: LoginDto) {
    return await this.authService.login(payload);
  }

  @Mutation(() => AuthResponse)
  async register(@Args('input') payload: RegisterDto) {
    return await this.authService.register(payload);
  }

  @Query(() => User)
  @UseGuards(GqlAuthGuard)
  async getUser(@GetUser() user: User, @GetFields() fields) {
    return user;
  }
}
