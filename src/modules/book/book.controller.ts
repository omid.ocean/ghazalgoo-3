import { Poet } from 'entity/poet.entity';

import { Controller, Get, Param, ParseIntPipe, Query } from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';

import { BookService } from './book.service';

@Controller('book')
@ApiTags('book')
export class BookController {
  constructor(private readonly service: BookService) {}

  @Get()
  @ApiQuery({ name: 'limit', required: false, example: 10 })
  @ApiQuery({ name: 'start', required: false, example: 1 })
  @ApiQuery({ name: 'end', required: false, example: 100 })
  @ApiQuery({ name: 'search', required: false, example: 'شاهنامه' })
  async getBooks(
    @Query('limit', new ParseIntPipe()) limit: number = 10,
    @Query('start') start?: number,
    @Query('end') end?: number,
    @Query('search') search: string = '',
  ) {
    return this.service.getBooks(start, end, limit, search, {
      relations: ['cover'],
      select: ['id', 'slogan', 'name'],
    });
  }

  @Get(':slug')
  @ApiResponse({
    status: 200,
    description: 'Book found',
    type: Poet,
  })
  async getBook(@Param('slug') slug: string) {
    return this.service.findBySlug(slug);
  }
}
