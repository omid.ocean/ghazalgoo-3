import { GetFields } from 'decorator/fields.decorator';
import { Roles } from 'decorator/roles.decorator';
import { GetUser } from 'decorator/user.decorator';
import { Book } from 'entity/book.entity';
import { BookComment } from 'entity/bookComment.entity';
import { BookCommentLike } from 'entity/bookCommentLike.entity';
import { BookLike } from 'entity/bookLike.entity';
import { Access } from 'entity/enum/Access.enum';
import { LikeAction } from 'entity/enum/likeAction.enum';
import { User } from 'entity/user.entity';
import { UpdateAndDelete } from 'generics/crud.dto';
import { GqlAuthGuard } from 'modules/auth/auth.guard';
import { RolesGuard } from 'modules/auth/roles.guard';
import { IAddAttr } from 'tools/IAddAttr.interface';
import { ID, Int, Mutation } from 'type-graphql';

import { UseGuards } from '@nestjs/common';
import { Args, Query, Resolver } from '@nestjs/graphql';

import { BookService } from './book.service';
import { CreateBookDTO } from './dto/CreateBook.dto';
import { PaginatedBookResponse } from './dto/pagination.dto';

@Resolver(() => Book)
export class BookResolver {
  constructor(private readonly service: BookService) {}

  @Query(() => Book)
  async getBook(
    @Args({ name: 'id', type: () => ID }) id: number,
    @GetFields() fields: IAddAttr,
  ) {
    return this.service.getBookById(id, fields);
  }

  @Query(() => PaginatedBookResponse)
  async getBooks(
    @Args({
      name: 'startCursor',
      type: () => ID,
      defaultValue: 0,
      description: 'start Cursor',
    })
    startCursor: number,
    @Args({
      name: 'endCursor',
      type: () => ID,
      nullable: true,
      description: 'end Cursor',
    })
    endCursor: number,
    @Args({
      name: 'limit',
      type: () => Int,
      nullable: true,
      defaultValue: 10,
      description: 'limits the amount of displayed items',
    })
    limit: number,
    @Args({
      name: 'search',
      nullable: true,
      defaultValue: '',
      description: 'optional search string used for searching book names',
      type: () => String,
    })
    search: string,
    @GetFields() fields: IAddAttr,
  ) {
    const poet = await this.service.getBooks(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
    );
    return poet;
  }

  @Query(() => UpdateAndDelete)
  @UseGuards(GqlAuthGuard, RolesGuard)
  @Roles(Access.ADMINISTRATOR)
  async editBook(
    @Args('input') payload: CreateBookDTO,
    @Args({ name: 'id', type: () => ID }) id: number,
  ) {
    return await this.service.edit(id, payload);
  }

  // fix me. figure out the bug about why mutation always crashes
  @Query(() => BookComment)
  @UseGuards(GqlAuthGuard)
  async addBookComment(
    @GetUser() user: User,
    @Args({ name: 'id', type: () => ID }) id: number,
    @Args({
      name: 'comment',
      nullable: false,
      type: () => String,
    })
    comment: string,
  ) {
    return this.service.addComment(id, user, comment);
  }

  @Query(() => BookCommentLike)
  @UseGuards(GqlAuthGuard)
  async likeBookComment(
    @GetUser() user: User,
    @Args({ name: 'id', type: () => ID }) id: number,
    @Args({
      name: 'action',
      nullable: false,
      type: () => LikeAction,
    })
    action: LikeAction,
  ) {
    return this.service.likeComment(id, user, action);
  }

  @Query(() => BookLike)
  @UseGuards(GqlAuthGuard)
  async likeBook(
    @GetUser() user: User,
    @Args({ name: 'id', type: () => ID }) id: number,
    @Args({
      name: 'action',
      nullable: false,
      type: () => LikeAction,
    })
    action: LikeAction,
  ) {
    return this.service.like(id, user, action);
  }
}
