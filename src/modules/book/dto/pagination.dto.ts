import { Book } from 'entity/book.entity';
import PaginatedResponse from 'generics/pagination.dto';
import { ObjectType } from 'type-graphql';

@ObjectType()
export class PaginatedBookResponse extends PaginatedResponse(Book) {}
