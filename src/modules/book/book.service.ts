import { LikeAction } from 'entity/enum/likeAction.enum';
import { User } from 'entity/user.entity';
import { IAddAttr } from 'tools/IAddAttr.interface';

import { Injectable, Logger } from '@nestjs/common';

import { BookRepository } from './book.repository';
import { CreateBookDTO } from './dto/CreateBook.dto';
import { PaginatedBookResponse } from './dto/pagination.dto';

@Injectable()
export class BookService {
  private readonly logger = new Logger(BookService.name);

  constructor(private readonly repository: BookRepository) {}

  getBookById(id: number, fields: IAddAttr) {
    return this.repository.findById(id, fields);
  }

  findBySlug(slogan: string) {
    return this.repository.findBySlug({
      where: { slogan },
      relations: ['cover', 'poet', 'chapters', 'chapters.poems'],
    });
  }

  async getBooks(
    startCursor: number,
    endCursor: number,
    limit: number,
    search: string,
    fields: IAddAttr,
  ): Promise<PaginatedBookResponse> {
    return await this.repository.getAllPaginated(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
    );
  }

  edit(id: number, payload: CreateBookDTO) {
    return this.repository.update(id, payload);
  }

  addComment(id: number, user: User, comment: string) {
    return this.repository.addComment(id, user, comment);
  }

  async like(id: number, user: User, action: LikeAction) {
    const like = await this.repository.like(id, user, action);
    return like;
  }

  likeComment(id: number, user: User, action: LikeAction) {
    return this.repository.likeComment(id, user, action);
  }
}
