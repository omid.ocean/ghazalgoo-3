import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BookController } from './book.controller';
import { BookRepository } from './book.repository';
import { BookResolver } from './book.resolver';
import { BookService } from './book.service';

@Module({
  controllers: [BookController],
  providers: [BookService, BookResolver],
  imports: [TypeOrmModule.forFeature([BookRepository])],
})
export class BookModule {}
