import { Book } from 'entity/book.entity';
import { LikeAction } from 'entity/enum/likeAction.enum';
import { User } from 'entity/user.entity';
import { RepoBase } from 'generics/baseRepository.repository';
import { IUserIntractable } from 'generics/userIntractable.interface';
import { IAddAttr } from 'tools/IAddAttr.interface';
import { EntityRepository } from 'typeorm';

import { BadRequestException } from '@nestjs/common';

import { BookComment } from '../../entity/bookComment.entity';
import { BookCommentLike } from '../../entity/bookCommentLike.entity';
import { BookLike } from '../../entity/bookLike.entity';

@EntityRepository(Book)
export class BookRepository extends RepoBase<Book> implements IUserIntractable {
  async addComment(id: string | number, user: User, commentText: string) {
    const book = await this.repository.findOne(id);
    if (!book) {
      throw new BadRequestException('invalid id!');
    }
    const comment = new BookComment();
    comment.comment = commentText;
    comment.createdBy = user;
    comment.book = book;
    return this.manager.save(comment);
  }

  async likeComment(id: string | number, user: User, action: LikeAction) {
    const comment = await this.manager.findOne(BookComment, id);
    if (!comment) {
      throw new BadRequestException('invalid id!');
    }
    const existingLike = await this.manager.findOne(BookCommentLike, {
      where: { blogPostComment: comment, createdBy: user },
    });
    if (!existingLike) {
      const like = new BookCommentLike();
      like.createdBy = user;
      like.comment = comment;
      like.action = action;
      return this.manager.save(like);
    }
    existingLike.action = action;
    return this.manager.save(existingLike);
  }

  async like(id: string | number, user: User, action: LikeAction) {
    const book = await this.repository.findOne(id);
    if (!book) {
      throw new BadRequestException('invalid id!');
    }
    const existingLike = await this.manager.findOne(BookLike, {
      where: { book, createdBy: user },
    });
    if (!existingLike) {
      const like = new BookLike();
      like.createdBy = user;
      like.book = book;
      like.action = action;
      return this.manager.save(like);
    }
    existingLike.action = action;
    return this.manager.save(existingLike);
  }

  getAllPaginated(
    startCursor: number,
    endCursor: number,
    limit: number,
    search: string,
    fields: IAddAttr,
  ) {
    return super.abstractGetAllPaginated(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
      'name',
    );
  }
}
