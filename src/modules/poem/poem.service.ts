import { IAddAttr } from 'tools/IAddAttr.interface';

import { Injectable, Logger } from '@nestjs/common';

import { PaginatedPoemResponse } from './dto/pagination.dto';
import { PoemRepository } from './poem.repository';

@Injectable()
export class PoemService {
  private readonly logger = new Logger(PoemService.name);

  constructor(private readonly repository: PoemRepository) {}

  getPoemById(id: number, fields: IAddAttr) {
    return this.repository.findById(id, fields);
  }

  findBySlug(slogan: string) {
    return this.repository.findBySlug({
      where: { slogan },
      relations: [
        'cover',
        'chapter',
        'chapter.poet',
        'verses',
        'tags',
        'comments',
        'likes',
      ],
    });
  }

  async getPoems(
    startCursor: number,
    endCursor: number,
    limit: number,
    search: string,
    fields: IAddAttr,
  ): Promise<PaginatedPoemResponse> {
    return await this.repository.getAllPaginated(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
    );
  }
}
