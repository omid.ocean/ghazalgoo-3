import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { PoemController } from './poem.controller';
import { PoemRepository } from './poem.repository';
import { PoemResolver } from './poem.resolver';
import { PoemService } from './poem.service';

@Module({
  controllers: [PoemController],
  providers: [PoemService, PoemResolver],
  imports: [TypeOrmModule.forFeature([PoemRepository])],
})
export class PoemModule {}
