import { Poem } from 'entity/poem.entity';
import { RepoBase } from 'generics/baseRepository.repository';
import { IAddAttr } from 'tools/IAddAttr.interface';
import { EntityRepository } from 'typeorm';

@EntityRepository(Poem)
export class PoemRepository extends RepoBase<Poem> {
  getAllPaginated(
    startCursor: number,
    endCursor: number,
    limit: number,
    search: string,
    fields: IAddAttr,
  ) {
    return super.abstractGetAllPaginated(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
      'name',
    );
  }
}
