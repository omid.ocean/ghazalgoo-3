import { GetFields } from 'decorator/fields.decorator';
import { Book } from 'entity/book.entity';
import { Poem } from 'entity/poem.entity';
import { IAddAttr } from 'tools/IAddAttr.interface';
import { Int } from 'type-graphql';

import { Args, Query, Resolver } from '@nestjs/graphql';

import { PaginatedPoemResponse } from './dto/pagination.dto';
import { PoemService } from './poem.service';

@Resolver(() => Poem)
export class PoemResolver {
  constructor(private readonly service: PoemService) {}

  @Query(() => Poem)
  async getPoem(
    @Args({ name: 'id', type: () => Int }) id: number,
    @GetFields() fields: IAddAttr,
  ) {
    return this.service.getPoemById(id, fields);
  }

  @Query(() => PaginatedPoemResponse)
  async getPoems(
    @Args({
      name: 'startCursor',
      type: () => Int,
      defaultValue: 0,
      description: 'start Cursor',
    })
    startCursor: number,
    @Args({
      name: 'endCursor',
      type: () => Int,
      nullable: true,
      description: 'end Cursor',
    })
    endCursor: number,
    @Args({
      name: 'limit',
      type: () => Int,
      nullable: true,
      defaultValue: 10,
      description: 'limits the amount of displayed items',
    })
    limit: number,
    @Args({
      name: 'search',
      nullable: true,
      defaultValue: '',
      description: 'optional search string used for searching poem names',
      type: () => String,
    })
    search: string,
    @GetFields() fields: IAddAttr,
  ) {
    const poet = await this.service.getPoems(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
    );
    return poet;
  }
}
