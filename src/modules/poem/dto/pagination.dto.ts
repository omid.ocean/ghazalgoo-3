import { Poem } from 'entity/poem.entity';
import PaginatedResponse from 'generics/pagination.dto';
import { ObjectType } from 'type-graphql';

@ObjectType()
export class PaginatedPoemResponse extends PaginatedResponse(Poem) {}
