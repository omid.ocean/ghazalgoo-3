import {
    IsArray, IsNumber, IsOptional, IsString, IsUUID, MaxLength, MinLength
} from 'class-validator';
import { Field, ID, InputType, Int } from 'type-graphql';

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

@InputType()
export class CreateChapterDTO {
  @Field({ nullable: false })
  @ApiProperty()
  @IsString()
  @MinLength(3)
  name: string;

  @Field({ nullable: false })
  @ApiProperty()
  @MinLength(25)
  @MaxLength(255)
  @IsString()
  description: string;

  @Field(() => ID)
  @ApiProperty()
  @IsUUID()
  cover: string;

  @Field(() => ID)
  @ApiProperty()
  @IsNumber()
  poet: number;

  @Field(() => [Int])
  @ApiPropertyOptional()
  @IsOptional()
  @IsArray()
  chapters: number[];

  @Field({ nullable: false })
  @ApiProperty()
  @MaxLength(70)
  @IsString()
  slogan: string;
}
