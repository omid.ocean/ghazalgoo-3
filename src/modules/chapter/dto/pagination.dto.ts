import { Chapter } from 'entity/chapter.entity';
import PaginatedResponse from 'generics/pagination.dto';
import { ObjectType } from 'type-graphql';

@ObjectType()
export class PaginatedChapterResponse extends PaginatedResponse(Chapter) {}
