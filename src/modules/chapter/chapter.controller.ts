import { Poet } from 'entity/poet.entity';

import { Controller, Get, Param, ParseIntPipe, Query } from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';

import { ChapterService } from './chapter.service';

@Controller('chapter')
@ApiTags('chapter')
export class ChapterController {
  constructor(private readonly service: ChapterService) {}

  @Get()
  @ApiQuery({ name: 'limit', required: false, example: 10 })
  @ApiQuery({ name: 'start', required: false, example: 1 })
  @ApiQuery({ name: 'end', required: false, example: 100 })
  @ApiQuery({ name: 'search', required: false, example: 'کتاب' })
  async getBooks(
    @Query('limit', new ParseIntPipe()) limit: number = 10,
    @Query('start') start?: number,
    @Query('end') end?: number,
    @Query('search') search: string = '',
  ) {
    return this.service.getChapters(start, end, limit, search, {
      relations: ['cover'],
      select: ['id', 'slogan', 'name', 'book'],
    });
  }

  @Get(':slug')
  @ApiResponse({
    status: 200,
    description: 'Book found',
    type: Poet,
  })
  async getBook(@Param('slug') slug: string) {
    return this.service.findBySlug(slug);
  }
}
