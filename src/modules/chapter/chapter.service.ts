import { IAddAttr } from 'tools/IAddAttr.interface';

import { Injectable, Logger } from '@nestjs/common';

import { ChapterRepository } from './chapter.repository';
import { PaginatedChapterResponse } from './dto/pagination.dto';

@Injectable()
export class ChapterService {
  private readonly logger = new Logger(ChapterService.name);

  constructor(private readonly repository: ChapterRepository) {}

  getChapterById(id: number, fields: IAddAttr) {
    return this.repository.findById(id, fields);
  }

  findBySlug(slogan: string) {
    return this.repository.findBySlug({
      where: { slogan },
      relations: ['cover', 'poet', 'book'],
    });
  }

  async getChapters(
    startCursor: number,
    endCursor: number,
    limit: number,
    search: string,
    fields: IAddAttr,
  ): Promise<PaginatedChapterResponse> {
    return await this.repository.getAllPaginated(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
    );
  }
}
