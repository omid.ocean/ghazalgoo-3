import { Chapter } from 'entity/chapter.entity';
import { RepoBase } from 'generics/baseRepository.repository';
import { IAddAttr } from 'tools/IAddAttr.interface';
import { EntityRepository } from 'typeorm';

@EntityRepository(Chapter)
export class ChapterRepository extends RepoBase<Chapter> {
  getAllPaginated(
    startCursor: number,
    endCursor: number,
    limit: number,
    search: string,
    fields: IAddAttr,
  ) {
    return super.abstractGetAllPaginated(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
      'name',
    );
  }
}
