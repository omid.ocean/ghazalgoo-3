import { GetFields } from 'decorator/fields.decorator';
import { Chapter } from 'entity/chapter.entity';
import { IAddAttr } from 'tools/IAddAttr.interface';
import { Int } from 'type-graphql';

import { Args, Query, Resolver } from '@nestjs/graphql';

import { ChapterService } from './chapter.service';
import { PaginatedChapterResponse } from './dto/pagination.dto';

@Resolver(() => Chapter)
export class ChapterResolver {
  constructor(private readonly chapterService: ChapterService) {}

  @Query(() => Chapter)
  async getChapter(
    @Args({ name: 'id', type: () => Int }) id: number,
    @GetFields() fields: IAddAttr,
  ) {
    return this.chapterService.getChapterById(id, fields);
  }

  @Query(() => PaginatedChapterResponse)
  async getChapters(
    @Args({
      name: 'startCursor',
      type: () => Int,
      defaultValue: 0,
      description: 'start Cursor',
    })
    startCursor: number,
    @Args({
      name: 'endCursor',
      type: () => Int,
      nullable: true,
      description: 'end Cursor',
    })
    endCursor: number,
    @Args({
      name: 'limit',
      type: () => Int,
      nullable: true,
      defaultValue: 10,
      description: 'limits the amount of displayed items',
    })
    limit: number,
    @Args({
      name: 'search',
      nullable: true,
      defaultValue: '',
      description: 'optional search string used for searching chapter names',
      type: () => String,
    })
    search: string,
    @GetFields() fields: IAddAttr,
  ) {
    const poet = await this.chapterService.getChapters(
      startCursor,
      endCursor,
      limit,
      search,
      fields,
    );
    return poet;
  }
}
