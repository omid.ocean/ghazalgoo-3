import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ChapterController } from './chapter.controller';
import { ChapterRepository } from './chapter.repository';
import { ChapterResolver } from './chapter.resolver';
import { ChapterService } from './chapter.service';

@Module({
  controllers: [ChapterController],
  providers: [ChapterService, ChapterResolver],
  imports: [TypeOrmModule.forFeature([ChapterRepository])],
})
export class ChapterModule {}
